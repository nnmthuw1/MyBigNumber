# proj/unittest/unittest.py

import unittest

from source.my_big_number import MyBigNumber

class testMyBigNumber(unittest.TestCase):
    def test_sum(self):
        # Testcase 1
        args = ("1234", "897")
        self.assertEqual(MyBigNumber.sum(*args), "2131", "Wrong!!!")
        # Testcase 2
        args = ("127490", "892751")
        self.assertEqual(MyBigNumber.sum(*args), "1020241", "Wrong!!!")
        # Testcase 3
        args = ("6920410256", "39999999999")
        self.assertEqual(MyBigNumber.sum(*args), "46920410255", "Wrong!!!")
        # Testcase 4
        args = ("927912086378291248", "125128829123425")
        self.assertEqual(MyBigNumber.sum(*args), "928037215207414673", "Wrong!!!")
        # Testcase 5
        args = ("87128492811512412424681924789125879124", "65890183906361242425325335134125819038590235362365235")
        self.assertEqual(MyBigNumber.sum(*args), "65890183906361329553818146646538243720515024488244359", "Wrong!!!")

if __name__ == "__main__":
    unittest.main()