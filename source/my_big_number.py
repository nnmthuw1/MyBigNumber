# proj/source/my_big_number.py
import logging
import argparse

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
 
class MyBigNumber:
    @staticmethod
    def sum(stn1: str, stn2: str):
        result = ''
        temp = 0
        ## Padding 0 at the left of number
        if len(stn1) < len(stn2):
            len_str = len(stn2)
            stn1 = stn1.rjust(len_str, '0')
        else:
            len_str = len(stn1)
            stn2 = stn2.rjust(len_str, '0')
        
        logging.info(f">>> Phép cộng hai số {stn1} và {stn2}")
        for i in range(len_str-1, -1, -1):
            s = int(stn1[i]) + int(stn2[i])
            logging.info(f"Lấy {stn1[i]} cộng với {stn2[i]} được {s}.")
            if temp > 0:
                s = s + temp
                logging.info(f"Cộng tiếp với nhớ {temp} được {s}")

            temp = int(s/10)
            result = str(s%10) + result
            logging.info(f"Lưu {s%10} vào kết quả được kết quả mới là \"{result}\"")
            if temp > 0:
                logging.info(f"Ghi nhớ {temp}.")

        if temp > 0:
            result = str(temp) + result
            logging.info(f"Cộng tiếp với nhớ {temp} được {temp}")
            logging.info(f"Lưu {temp} vào kết quả được kết quả mới là \"{result}\"")

        logging.info(f">>> Kết quả phép toán: {stn1} + {stn2} = {result}\n")
        return result
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'Tổng hai số tự nhiên')
    parser.add_argument('stn1', help='Số tự nhiên 1')
    parser.add_argument('stn2', help='Số tự nhiên 2')
    args = parser.parse_args()
    MyBigNumber.sum(args.stn1, args.stn2)