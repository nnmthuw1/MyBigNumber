# MyBigNumber



## Descriptions

The repository contains the **MyBigNumber** class, which includes a method **sum** for summing two large string numbers, and unit tests for verifying the functionality of this summing method.

## Requirements

```python
python==3.8.16
```
## Getting Started

Clone repository from [My Big Number project](#https://gitlab.com/nnmthuw1/MyBigNumber.git).

```bash
git clone https://gitlab.com/nnmthuw1/MyBigNumber.git
cd MyBigNumber
```

### Run Sum Method
Example the sum of "8791278491" and "2362365142"

```bash
python source/my_big_number.py 8791278491 2362365142
```

### Run Unit Testing
Using unit testing have 5 test cases from small to big string number.

+ 1234 + 897
+ 127490 + 892751
+ 6920410256 + 39999999999
+ 927912086378291248 + 125128829123425
+ 87128492811512412424681924789125879124 + 65890183906361242425325335134125819038590235362365235

```bash
python -m test.unittest
```

## Contributors 

> Nguyen Nhat Minh Thu